var Y = require('./core');

function normalize(val){
    var type = typeof val;
    if(type === 'string'){
        return val;
    }else if(type === 'number'){
        return isFinite(val) ? val : '';
    }else if(type === 'boolean'){
        return val;
    }else{
        return '';
    }
}

Y.querystring = {
    stringify: function(obj) {
        var value;
        if(typeof obj === 'object'){
            return Object.keys(obj).map(function(key){
                value = obj[key];
                key = encodeURIComponent(key);
                if(Array.isArray(value)){
                    return value.map(function(val){
                        return key + '=' + encodeURIComponent(normalize(val));
                    }).join('&');
                }else{
                    return key + '=' + encodeURIComponent(normalize(value));
                }
            }).join('&');
        }else{
            return '';
        }
    },
    parse: function(str){
        str += '';
        if(!str){
            return {};
        }
        var pairs = str.split('&'),
            obj = {},
            key,
            value;
        pairs.forEach(function(pair){
            if(!pair){
                return;
            }
            pair = pair.split('=');
            key = decodeURIComponent(pair[0]),
            value = decodeURIComponent(pair[1] || '');
            if(obj[key] === undefined){
                obj[key] = value;
            }else{
                if(Array.isArray(obj[key])){
                    obj[key].push(value);
                }else{
                    obj[key] = [obj[key],value];
                }
            }
        });
        return obj;
    }
};


module.exports = Y;
