var Y = require('./core');

Y.EP = function(){
    this.callbacks = {};
};

Y.extend(Y.EP.prototype,{
    on:function(event,callback){
        if(typeof callback !== 'function'){
            return;
        }else{
            this.callbacks[event] = this.callbacks[event] || [];
            this.callbacks[event].push(callback);
        }
        return this;
    },
    emit:function(event,args,context){
        var cbs = this.callbacks[event],
            len = cbs.length,
            i;
        context = context || this;

        if(!cbs){
            return this;
        }

        for (i = 0; i < len; i++) {
            setTimeout(function(callback){
                callback.apply(context, args);
            },0,cbs[i]);
        }
        return this;
    },
    off:function(event,callback){
        var cbs = this.callbacks[event],
            len = cbs.length,
            i;
        if(!cbs){
            return this;
        }
        if(typeof callback === 'function'){
            for(i=0;i<len;i++){
                if(cbs[i] === callback){
                    cbs.splice(i,1);
                    len --;
                }
            }
        }else{
            cbs.length = 0;
        }
        return this;
    }
});

module.exports = Y;
