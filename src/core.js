'use strict';

var Y = function() {};

Y.fn = Y.prototype;


var class2type = {};


Y.extend = function(target, source) {
    var keys,
        key,
        i;
    if (typeof source !== 'object') {
        source = target;
        target = this;
    }
    keys = Y.keys(source);
    for (i = 0; i < keys.length; i++) {
        key = keys[i];
        if(target[key] === source[key]){
            continue;
        }else{
            target[key] = source[key];
        }
    }
    return target;
};

Y.keys = function(obj) {
    var ret = [],
        key;
    if (!Y.isObject(obj)) {
        return [];
    }
    if (Object.keys) {
        return Object.keys(obj);
    }
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            ret.push(key);
        }
    }
    return ret;
};


'Boolean String Array Number Function Date RegExp Error Object'.split(' ').forEach(function(type) {
    class2type['[object ' + type + ']'] = type.toLowerCase();
});

Y.type = function(obj) {
    if (obj == null) {
        return obj + '';
    }
    return typeof obj === 'object' || typeof obj === 'function' ?
        class2type[toString.call(obj)] || typeof obj :
        typeof obj;
};

Y.isObject = function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
};



Y.isArray = Array.isArray || function(obj) {
    return Y.type(obj) === 'array';
};


var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
Y.isArrayLike = function(obj) {
    var length;
    if (obj != null) {
        length = obj.length;
    }
    return typeof length === 'number' && length >= 0 &&
        length <= MAX_ARRAY_INDEX;
};

Y.identity = function(value) {
    return value;
};

Y.each = function(obj, iteratee) {
    var i,
        length,
        keys;

    if (Y.isArrayLike(obj)) {
        for (i = 0, length = obj.length; i < length; i++) {
            if (iteratee(obj[i], i, obj) === false) {
                break;
            }
        }
    } else {
        keys = Y.keys(obj);
        for (i = 0, length = keys.length; i < length; i++) {
            if (iteratee(obj[keys[i]], keys[i], obj) === false) {
                break;
            }
        }
    }
    return obj;
};
Y.reduce = function(arr, iteratee, base) {
    var ret = base || 0,
        i,
        length = arr.length;
    for (i = 0; i < length; i++) {
        ret = iteratee(ret, arr[i], i, arr);
    }
    return ret;
};

Y.all = Y.every = function(arr, predicate) {
    var ret = true;
    Y.each(arr, function(elem, index, arr) {
        if (!predicate(elem, index, arr)) {
            ret = false;
            return false;
        }
    });
    return ret;
};

Y.any = Y.some = function(arr, predicate) {
    var ret = false;
    Y.each(arr, function(elem, index, arr) {
        if (predicate(elem, index, arr)) {
            ret = true;
            return false;
        }
    });
    return ret;
};

Y.map = function(arr, iteratee) {
    var ret = [];
    Y.each(arr, function(elem, index, arr) {
        ret.push(iteratee(elem, index, arr));
    });
    return ret;
};

Y.filter = function(arr, predicate) {
    var ret = [];
    Y.each(arr, function(elem, index, arr) {
        if (predicate(elem, index, arr)) {
            ret.push(elem);
        }
    });
    return ret;
};

Y.find = function(arr, predicate) {
    var ret;
    Y.each(arr, function(value, index, arr) {
        if (predicate(value, index, arr)) {
            ret = value;
            // break each
            return false;
        }
    });
    return ret;
};


Y.values = function(obj) {
    var ret = [],
        keys = Y.keys(obj);
    Y.each(keys, function(key) {
        ret.push(obj[key]);
    });
    return ret;
};

Y.pairs = function(obj) {
    var keys = Y.keys(obj),
        ret = [];
    Y.each(keys, function(key) {
        ret.push([key, obj[key]]);
    });
    return ret;
};

Y.object = function(arr) {
    var ret = {};
    Y.each(arr, function(value) {
        ret[value[0]] = value[1];
    });
    return ret;
};

Y.pluck = function(arr, key) {
    var ret = [];
    if (!Y.isArray(arr)) {
        return new Error('Excepting an Array but ' + Y.type(arr) + 'given');
    }
    Y.each(arr, function(obj) {
        ret.push(obj[key]);
    });
    return ret;
};

Y.merge = function(target,source){
    var length = source.length,
        i,
        j = target.length;
    for(i=0;i<length;i++){
        target[j] = source[i];
        j++;
    }
    target.length = j;
    return target;
};

Y.remove = function(obj /*arr*/,keys /*arr*/){
    var type = Y.type(obj),
        i,
        pos,
        length = keys.length;
    if(type === 'object'){
        for(i=0;i<length;i++){
            if(keys[i] in obj){
                delete obj[keys[i]];
            }
        }
    }else if(type === 'array'){
        for(i=0;i<length;i++){
            pos = obj.indexOf(keys[i]);
            if(pos !== -1){
                obj.splice(pos,1);
            }
        }
    }
    return obj;
};

Y.omit = function(obj, omitKeys) {
    var ret = {},
        i,
        length,
        keys = Y.keys(obj);

    Y.remove(keys,omitKeys);
    for (i = 0,length = keys.length; i < length; i++) {
        ret[keys[i]] = obj[keys[i]];
    }

    return ret;
};

Y.pick = function(obj, keys) {
    var ret = {},
        i,
        length = keys.length;
    for (i = 0; i < length; i++) {
        if (keys[i] in obj) {
            ret[keys[i]] = obj[keys[i]];
        }
    }
    return ret;
};

// run function n times
Y.times = function(n, fn,args,context) {
    for (var i = 0; i < n; i++) {
        fn.apply(context,args);
    }
};

// get a randon int in range [min,max]
Y.random = function(min, max) {
    if (max == null) {
        max = min;
        min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
};

Y.copy = function(obj) {
    if (Y.isArray(obj)) {
        return obj.slice();
    } else if (Y.isObject(obj)) {
        return Y.extend({}, obj);
    } else {
        return obj;
    }
};

Y.unique = function(arr){
    var copy = arr.slice(),
        ret,
        i,
        len = copy.length;
    copy.sort();
    ret = [copy[0]];
    for(i=1;i<len;i++){
        if(copy[i] !== copy[i-1]){
            ret.push(copy[i]);
        }
    }
    return ret;
};

module.exports = Y;
