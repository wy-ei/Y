var Y = require('../src/index');
var test = require('tape');

test('querystring', function(t) {
    var querystring = Y.querystring;

    var testCases = [
        ['', {}],
        ['foo=bar&foo=baz', {'foo': ['bar', 'baz']}],
        ['foo=bar', {'foo': 'bar'}],
        ['a=a&b=b&c=c', { a: 'a', b: 'b', c: 'c' }],
        ['foo=bar&arr%5B%5D=1',{'foo': 'bar', 'arr[]': '1'}],
    ];

    testCases.forEach(function(testCase){
        t.deepEqual(querystring.stringify(testCase[1]), testCase[0]);
        t.deepEqual(querystring.parse(testCase[0]), testCase[1]);
    });

    t.deepEqual(querystring.parse('foo&bar'), { foo: '', bar: '' });
    t.deepEqual(querystring.stringify({ foo: '', bar: '' }), 'foo=&bar=');

    t.end();
});
