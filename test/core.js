var Y = require('../src/index');
var test = require('tape');

test('extend', function(t) {
    var obj1 = {},
        obj2 = {
            a: 1,
            b: 2
        };

    Y.extend(obj1, obj2);
    t.equal(obj1.a === 1 && obj1.b === 2, true);

    Y.extend(obj1);
    t.equal(Y.a === 1 && Y.b === 2, true);

    t.end();
});


test('type', function(t) {
    var boolean = true,
        str = '',
        arr = [],
        number = 1,
        func = function() {},
        date = new Date,
        reg = /1/,
        err = new Error,
        obj = {};
    t.equal(Y.type(boolean), 'boolean');
    t.equal(Y.type(str), 'string');
    t.equal(Y.type(arr), 'array');
    t.equal(Y.type(number), 'number');
    t.equal(Y.type(func), 'function');
    t.equal(Y.type(date), 'date');
    t.equal(Y.type(reg), 'regexp');
    t.equal(Y.type(err), 'error');
    t.equal(Y.type(obj), 'object');

    t.end();
});


test('each', function(t) {
    var arr = [1, 2, 3],
        obj = {
            a: 1,
            b: 2,
            c: 3
        };
    Y.each(arr, function(elem, i, arr) {
        arr[i] = elem + 1;
    });

    t.deepEqual(arr, [2, 3, 4], 'array each is Ok');

    Y.each(obj, function(value, key, obj) {
        obj[key] = value + 1;
    });

    t.deepEqual(obj, {
        a: 2,
        b: 3,
        c: 4
    }, 'object each is Ok');

    t.end();
});

test('isArray', function(t) {
    var arr = [1, 2, 3],
        arrLike = {
            '0': 0,
            '1': 1,
            '2': 2,
            length: 3
        };
    t.equal(Y.isArray(arr), true);
    t.equal(Y.isArray(arrLike), false);
    t.equal(Y.isArray('abc'), false);
    t.equal(Y.isArray(1), false);

    t.end();
});

test('isArrayLike', function(t) {
    var arr = [1, 2, 3],
        arrLike = {
            0: 0,
            1: 1,
            2: 2,
            length: 3
        };
    t.equal(Y.isArrayLike(), false, 'false value is not arrayLike');
    t.equal(Y.isArrayLike(''), true, 'empty string is ArrayLike');
    t.equal(Y.isArrayLike(arr), true, 'array is ArrayLike');
    t.equal(Y.isArrayLike(arrLike), true, 'arrLike obj is ArrayLike');
    t.equal(Y.isArrayLike('abc'), true, 'string is ArrayLike');
    t.equal(Y.isArrayLike(1), false, 'number is not arrayLike');

    t.end();
});


test('reduce', function(t) {
    var arr = [1, 2, 3],
        sum = 0;

    sum = Y.reduce(arr, function(base, val) {
        return base + val;
    });

    t.equal(sum, 6, 'reduce is ok');

    t.end();
});

test('all', function(t) {
    var arr = [0, 2, 4],
        arrLike = {
            0: 0,
            1: 2,
            length: 2
        },
        result;

    result = Y.all(arr, function(elem) {
        return elem % 2 === 0;
    });

    t.equal(result, true, 'all for array');

    result = Y.all(arrLike, function(elem, index) {
        return elem === 2 * index;
    });

    t.equal(result, true, 'all for arrayLike');

    t.end();
});

test('any', function(t) {
    var arr = [0, 2, 4],
        arrLike = {
            0: 0,
            1: 2,
            length: 2
        },
        result;

    result = Y.any(arr, function(elem) {
        return elem === 2;
    });
    t.equal(result, true, 'any for array');

    result = Y.any(arrLike, function(elem) {
        return elem === 3;
    });
    t.equal(result, false, 'any for arrlke');

    result = Y.any(arrLike, function(elem) {
        return elem === 2;
    });
    t.equal(result, true, 'any for arrlke');

    t.end();
});

test('map', function(t) {
    var arr = [0, 2, 4],
        arrLike = {
            0: 0,
            1: 2,
            2: 4,
            length: 3
        },
        result;

    result = Y.map(arr, function(elem) {
        return elem * 2;
    });
    t.deepEqual(result, [0, 4, 8], 'map for array');

    result = Y.map(arrLike, function(elem) {
        return elem * 2;
    });
    t.deepEqual(result, [0, 4, 8], 'map for arrlke');

    t.end();
});


test('filter', function(t) {
    var arr = [0, 2, 4],
        result;

    result = Y.filter(arr, function(elem) {
        return elem >= 2;
    });
    t.deepEqual(result, [2, 4], 'fliter for array');

    t.end();
});

test('find', function(t) {
    var arr = [{
            name: 'james'
        }, {
            name: 'kobe'
        }],
        result;

    result = Y.find(arr, function(elem) {
        return elem.name === 'james';
    });
    t.deepEqual(result, {
        name: 'james'
    }, 'find for array');

    t.end();
});

test('values', function(t) {
    var obj = {
        a: 1,
        b: 2,
        c: 3
    };

    t.deepEqual(Y.values(obj), [1, 2, 3], 'values for array');
    t.deepEqual(Y.values(), [], 'values for undefined');

    t.end();
});

test('pluck', function(t) {
    var arr = [{
            name: 'james'
        }, {
            name: 'kobe'
        }],
        result;

    result = Y.pluck(arr, 'name');
    t.deepEqual(result, ['james', 'kobe'], 'pluck for array');

    t.end();
});


test('merge', function(t) {
    var arr = [1, 2, 3],
        result;

    result = Y.merge(arr, [4, 5, 6]);
    t.deepEqual(result, [1, 2, 3, 4, 5, 6], 'merge two array');

    t.end();
});

test('remove', function(t) {
    var arr = [1, 2, 3],
        obj = {
            a: 1,
            b: 2,
            c: 3
        },
        result;

    result = Y.remove(arr, [1, 2]);
    t.deepEqual(result, [3], 'remove an array from another array');

    result = Y.remove(obj, ['a', 'b']);
    t.deepEqual(result, {
        c: 3
    }, 'remove some property from object');


    t.end();
});


test('omit', function(t) {
    var obj = {
            a: 1,
            b: 2,
            c: 3
        },
        result;

    result = Y.omit(obj, ['a', 'b']);
    t.deepEqual(result, {
        c: 3
    }, 'omit some property from object');

    t.end();
});

test('pick', function(t) {
    var obj = {
            a: 1,
            b: 2,
            c: 3
        },
        result;

    result = Y.pick(obj, ['a', 'b']);
    t.deepEqual(result, {
        a: 1,
        b: 2
    }, 'pick some property from object');

    t.end();
});


test('times', function(t) {
    var arr = [],
        obj = {
            a: 0
        };
    Y.times(3, function(b) {
        arr.push(this.a + b);
    }, [1], obj);

    t.deepEqual(arr, [1, 1, 1], 'run function n times');

    t.end();
});

test('unique', function(t) {
    var arr = [1,3,4,3,2,3,1,3,2,4,5,3,4,5];
    var ret = Y.unique(arr);

    t.deepEqual(ret, [1, 2, 3, 4, 5], 'unique array');

    t.end();
});
