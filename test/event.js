var Y = require('../src/index');
var test = require('tape');

test('observer pattern', function(t) {
    var a = 0,
        b = {},
        obj = {
            c: 1,
            d: 2
        };

    var ep = new Y.EP;
    ep.on('a', function() {
        a += 1;
    });
    ep.emit('a');

    setTimeout(function() {
        t.equal(a === 1, true, 'bind event success');
    });

    ep.off('a');
    ep.emit('a');
    setTimeout(function() {
        t.equal(a === 1, true, 'off event success');
    });
    ep.on('b', function(e, f) {
        b = {
            c: this.c,
            d: this.d,
            e: e,
            f: f
        };
    });
    ep.emit('b', [3, 4], obj);
    setTimeout(function() {
        t.equal(b.c === 1 && b.d === 2 &&
            b.e === 3 && b.f === 4,
            true,
            'emit event success');
    });
    t.end();
});
